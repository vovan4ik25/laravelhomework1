<?php

use Illuminate\Database\Seeder;

class TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
                array(
                    'title' => 'Джинсы Levis',
                    'alias' => 'jeans',
                    'price' => 899,
                    'description' => 'Levi’s – американский бренд, под именем которого были созданы и запатентованы первые в мире джинсы. Основан в 1873 году в городе Сан-Франциско (США). Специализируется на производстве одежды (в первую очередь джинсовой), обуви и аксессуаров.',
                    'category_id' => 1
                ),
                array(
                    'title' => 'Торт',
                    'alias' => 'tort',
                    'price' => 200,
                    'description' => 'Торт, торт, торт.',
                    'category_id' => 2
                ),
                array(
                    'title' => 'Вода',
                    'alias' => 'voda',
                    'price' => 50,
                    'description' => 'Вода, вода, вода.',
                    'category_id' => 3
                ),
                array(
                    'title' => 'Рубашка Oodji',
                    'alias' => 'shirt',
                    'price' => 555,
                    'description' => 'Oodji (Оджи) — бренд одежды, который в 2010 году переименовывался c Oggi в Oodji. Данная марка пользуется большой популярностью среди молодежи.',
                    'category_id' => 1
                ),
                array(
                    'title' => 'Часы CASIO',
                    'alias' => 'clock',
                    'price' => 2200,
                    'description' => 'Коллекция Standard Analogue - это наручные часы с аналоговой индикацией времени классического вида, с кожаными ремешками или с браслетами из нержавеющей стали. Часы в данной коллекции отличаются по цене и функциональным возможностям - от доступных классических моделей до сложных хронографов и моделей с дополнительной защитой для настоящих дайверов. Большинство моделей изготавливаются из нержавеющей стали, что обеспечивает им прочность и долговечность.',
                    'category_id' => 1
                )]
        );

        DB::table('categories')->insert([
                array(
                    'category' => 'clothes',
                    'alias' => 'clothes'
                ),
                array(
                    'category' => 'food',
                    'alias' => 'food'
                ),
                array(
                    'category' => 'drinks',
                    'alias' => 'drinks'
                )]
        );

        DB::table('users')->insert([
                array(
                    'name' => 'Vova',
                    'email' => 'vova@mail.ru',
                    'password' => '$2y$10$mUK.HbwaADn3PbQFLJka5u1mJJQS9p8Yajs17IoAJZ.V41dVaHwVq'
                ),
                array(
                    'name' => 'Admin',
                    'email' => 'admin@mail.com',
                    'password' => '$2y$10$iVR4Kv3MaflJYTrSFI1jqeRt6cAmbM1TnbJPpstxuc4uT.MNGmR22'
                )]
        );

        DB::table('orders')->insert([
                array(
                    'customer_name' => 'Vova',
                    'email' => 'vova@gmail.com',
                    'phone' => 23542895,
                    'feedback' => 'well'
                ),
                array(
                    'customer_name' => 'Sasha',
                    'email' => 'sasha@gmail.com',
                    'phone' => 35429872,
                    'feedback' => 'well'
                ),
                array(
                    'customer_name' => 'Masha',
                    'email' => 'masha@gmail.com',
                    'phone' => 65435895,
                    'feedback' => 'commonly'
                )]
        );

        DB::table('pages')->insert([
                array(
                    'title' => 'Php is awesome',
                    'alias' => 'php',
                    'intro' => 'The PHP development team announces the immediate availability of PHP 7.1.5. Several bugs have been fixed. All PHP 7.1 users are encouraged to upgrade to this version.',
                    'content' => 'The PHP development team announces the immediate availability of PHP 7.1.5. Several bugs have been fixed. All PHP 7.1 users are encouraged to upgrade to this version.For source downloads of PHP 7.1.5 please visit our downloads page, Windows source and binaries can be found on windows.php.net/download/. The list of changes is recorded in the ChangeLog.'
                ),
                array(
                    'title' => 'Laravel 5.4 released!',
                    'alias' => 'laravel',
                    'intro' => 'Love beautiful code? We do too.',
                    'content' => 'Value elegance, simplicity, and readability? You’ll fit right in. Laravel is designed for people just like you. If you need help getting started, check out Laracasts and our great documentation.'
                ),
                array(
                    'title' => 'Seeders helps us to fill database',
                    'alias' => 'seeders',
                    'intro' => 'Love beautiful code',
                    'content' => 'Value elegance, simplicity, and readability? You’ll fit right in. Laravel is designed for people just like you. If you need help getting started, check out Laracasts and our great documentation.'
                )]
        );
    }
}
