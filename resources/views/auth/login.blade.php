@extends('layouts.layout')

@section('headerBlock')
    <div class="container">
        <h1>Sign in:</h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-push-2">
            <form method="post" action="/login">

                {{ csrf_field() }}

                <div class="form-group">
                    <label for="email">Email:</label>
                    <input name="email" type="email" id="email" class="form-control">
                </div>

                <div class="form-group">
                    <label for="password">Password:</label>
                    <input name="password" type="password" id="password" class="form-control">
                </div>

                <div class="form-group">
                    <button class="btn btn-primary">Sign in</button>
                </div>
            </form>

            @include('layouts.formError')

        </div>
    </div>
@endsection