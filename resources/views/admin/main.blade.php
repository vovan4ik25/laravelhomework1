@extends('layouts.layoutAdmin')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <a class="btn btn-default" href="/admin/products" role="button">Products</a>
            <a class="btn btn-default" href="/admin/pages" role="button">Pages</a>
        </div>

    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1>Main Admin</h1>
    </div>
@endsection