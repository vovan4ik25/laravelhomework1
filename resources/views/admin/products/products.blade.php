@extends('layouts.layoutAdmin')

@section('content')
    <div class="row">
        @foreach($products AS $product)
            <div class="col-md-12">
                <h2>{{$product->title}}</h2>
                <p><a class="btn btn-warning" href="/admin/products/{{$product->alias}}/edit" role="button">Редактировать »</a></p>
                <p><form action="/admin/products/{{$product->alias}}" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="Удалить »" class="btn btn-danger">
                </form></p>
            </div>
        @endforeach
        <a class="navbar-brand" href="/admin/products/create">New products</a>
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1>Products Admin</h1>
    </div>
@endsection