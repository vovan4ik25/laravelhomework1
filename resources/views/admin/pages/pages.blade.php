@extends('layouts.layoutAdmin')

@section('content')
    <div class="row">
        @foreach($pages AS $page)
            <div class="col-md-12">
                <h2>{{$page->title}}</h2>
                <p>{{$page->intro}}</p>
                <p><a class="btn btn-warning" href="/admin/pages/{{$page->alias}}/edit" role="button">Редактировать »</a></p>
                <p><form action="/admin/pages/{{$page->alias}}" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="Удалить »" class="btn btn-danger">
                </form></p>
            </div>
        @endforeach
                <a class="navbar-brand" href="/admin/pages/create">New pages</a>
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1>Pages Admin</h1>
    </div>
@endsection