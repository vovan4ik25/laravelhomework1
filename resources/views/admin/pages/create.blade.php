@extends('layouts.layoutAdmin')

@section('headerBlock')
    <div class="container">
        <h1>Create new page:</h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-push-2">
            <form method="post" action="/admin/pages">

                <input type="hidden" name="_method" value="PUT">

                {{ csrf_field() }}

                <div class="form-group">
                    <label for="title">Title:</label>
                    <input name="title" type="text" id="title" class="form-control">
                </div>

                <div class="form-group">
                    <label for="alias">Alias:</label>
                    <input name="alias" type="text" id="alias" class="form-control">
                </div>

                <div class="form-group">
                    <label for="intro">Intro:</label>
                    <input name="intro" type="text" id="intro" class="form-control">
                </div>

                <div class="form-group">
                    <label for="content">Content:</label>
                    <textarea name="content" id="content" class="form-control"></textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary">Submit</button>
                </div>
            </form>

            @include('layouts.formError')

        </div>
    </div>
@endsection