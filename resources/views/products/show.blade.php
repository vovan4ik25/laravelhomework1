@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <p>{{$product->description}}</p>
            <p>{{'Price: '.$product->price.' UAH'}}</p>
        </div>
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1>{{$product->title}}</h1>
    </div>
@endsection