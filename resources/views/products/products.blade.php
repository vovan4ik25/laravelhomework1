@extends('layouts.layout')

@section('content')
    <div class="col-md-12">
        <h3>Выбрать категорию</h3>
            @foreach($category AS $categor)
            <h3 style="float: left; margin: 5px"><a class="btn btn-primary active" href="/category/{{$categor->alias}}" role="button">{{$categor->category}} »</a></h3>
            @endforeach
        <br>
        <br>
        <hr>
    </div>
    <div class="row">
        @foreach($products AS $product)
            <div class="col-md-12">
                <h2>{{$product->title}}</h2>
                <p><a class="btn btn-default" href="/products/{{$product->alias}}" role="button">Читать далее »</a></p>
            </div>
        @endforeach

    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1>Products User</h1>
    </div>
@endsection