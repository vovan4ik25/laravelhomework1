@extends('layouts.layout')

@section('content')
    <div class="row">
        @foreach($category->products AS $product)
            <div class="col-md-12">
                <h2>{{$product->title}}</h2>
                <p><a class="btn btn-default" href="/products/{{$product->alias}}" role="button">Читать далее »</a></p>
            </div>
        @endforeach
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1></h1>
    </div>
@endsection