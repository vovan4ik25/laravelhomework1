@extends('layouts.layout')

@section('content')

    <div class="row">
        @foreach($category AS $categor)
            <div class="col-md-12">
                <p><a class="btn btn-default" href="/category/{{$categor->alias}}" role="button"> {{$categor->category}} >></a></p>
            </div>
        @endforeach
    </div>

@endsection

@section('headerBlock')
    <div class="container">
        <h1>Category user</h1>
    </div>
@endsection