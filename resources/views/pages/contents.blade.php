@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2>{{$page->title}}</h2>
            <p>{{$page->content}}</p>
        </div>
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1>Text</h1>
    </div>
@endsection