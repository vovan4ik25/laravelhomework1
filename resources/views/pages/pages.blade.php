@extends('layouts.layout')

@section('content')
    <div class="row">
        @foreach($pages AS $page)
            <div class="col-md-12">
                <h2>{{$page->title}}</h2>
                <p>{{$page->intro}}</p>
                <p><a class="btn btn-default" href="/pages/{{$page->alias}}" role="button">Читать далее »</a></p>
            </div>
        @endforeach
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1>Pages User</h1>
    </div>
@endsection