@extends('layouts.layout')

@section('content')
    <div class="row">
        @foreach($orders AS $order)
            <div class="col-md-12">
                <h2>{{$order->customer_name}}</h2>
                <p>{{'Email: '.$order->email}}</p>
                <p>{{'Phone: '.$order->phone}}</p>
                <p>{{'Feedback: '.$order->feedback}}</p>
            </div>
        @endforeach
    </div>
@endsection

@section('headerBlock')
    <div class="container">
        <h1>Orders</h1>
    </div>
@endsection