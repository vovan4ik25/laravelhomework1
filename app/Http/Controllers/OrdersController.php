<?php

namespace App\Http\Controllers;

use App\Order as Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{

    public function orders()
    {

        $data['orders'] = Order::all();

        return view('orders.orders', $data);
    }

}
