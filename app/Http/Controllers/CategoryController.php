<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function category()
    {

        $data['category'] = Category::all();

        return view('category.category', $data);

    }


    public function show(Category $category){

        return view('category.show')->with(compact('category'));

    }

}
