<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{

    public function products()
    {

        $data['products'] = Product::all();

        $data['category'] = Category::all();

        return view('index', $data);

    }


    public function productsContents(Product $product){

        return view('products.show')->with(compact('product'));

    }

}
