<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{

    public function create()
    {

    return view('auth.register');

    }


    public function store()
    {
        $this->validate(request(),[
            'name' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed'
        ]);

        $user = User::create(
            array_merge(
                request(['name','email']),
                ['password' => bcrypt(request('password'))]
            )
        );

        auth()->login($user);

        return redirect('/');
    }

}
