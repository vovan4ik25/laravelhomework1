<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{

    public function create()
    {

        return view('auth.login');

    }


    public function store()
    {
        if(!auth()->attempt(request(['email', 'password']))){
            return back();
        }
        if(request('email') == 'admin@mail.com'){
            return view('admin.main');
        }
        return redirect('/');
    }


    public function destroy()
    {
        auth()->logout();
        return redirect('/');
    }

}
