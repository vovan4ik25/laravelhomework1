<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{

    public function products()
    {

        $data['products'] = Product::all();

        return view('admin.products.products', $data);

    }


    public function create()
    {

        $data['category'] = Category::all();

        return view('admin.products.create', $data);

    }


    public function store()
    {
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'price' => 'required|numeric',
            'description' => 'required',
            'category_id' => 'required',
        ]);

        Product::create(request(['title', 'alias', 'price', 'description', 'category_id']));

        return redirect('/admin/products');

    }


    public function edit(Product $product)
    {

        $data['category'] = Category::all();

        return view('admin.products.edit', compact('product'), $data);

    }

    public function update(Product $product)
    {
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'price' => 'required|numeric',
            'description' => 'required',
            'category_id' => 'required',
        ]);

        $product->update(request()->all());

        return redirect('/admin/products');

    }


    public function destroy(Product $product)
    {

        $product->delete();

        return redirect('/admin/products');

    }

}
