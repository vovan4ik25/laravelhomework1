<?php

namespace App\Http\Controllers\Admin;

use App\Page as Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function pages()
    {

        $data['pages'] = Page::all();

        return view('admin.pages.pages', $data);

    }


    public function create(){

        return view('admin.pages.create');

    }


    public function store()
    {
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'intro' => 'required',
            'content' => 'required',
        ]);

        Page::create(request(['title', 'alias', 'intro', 'content']));

        return redirect('/admin/pages');

    }


    public function edit(Page $page)
    {

        return view('admin.pages.edit', compact('page'));

    }


    public function update(Page $page)
    {
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'intro' => 'required',
            'content' => 'required',
        ]);

        $page->update(request()->all());

        return redirect('/admin/pages');

    }


    public function destroy(Page $page)
    {

        $page->delete();

        return redirect('/admin/pages');

    }

}
