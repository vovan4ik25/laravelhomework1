<?php

namespace App\Http\Controllers;

use App\Page as Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{

    public function pages()
    {

        $data['pages'] = Page::all();

        return view('pages.pages', $data);

    }


    public function pagesContents(Page $page)
    {

        $data['page'] = $page;

        return view('pages.contents')->with($data);

    }

}
