<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = ['alias', 'category'];


    public function getRouteKeyName()
    {
        return 'alias';
    }


    public function products(){

        return $this->hasMany(Product::class);

    }


}
