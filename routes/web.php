<?php


Route::get('/', "ProductsController@products");


Route::get('/main', "Admin\MainController@index");

Route::get('/admin/products', "Admin\ProductsController@products");

Route::get('/admin/products/create', "Admin\ProductsController@create");

Route::get('/admin/products/{product}/edit', "Admin\ProductsController@edit");

Route::post('/admin/products/{product}', "Admin\ProductsController@update");

Route::delete('/admin/products/{product}', "Admin\ProductsController@destroy");

Route::put('/admin/products',"Admin\ProductsController@store");



Route::get('/products/{product}',"ProductsController@productsContents");



Route::get('/category', "CategoryController@category");

Route::get('/category/{category}', "CategoryController@show");



Route::get('/orders',"OrdersController@orders");



Route::get('/admin/pages',"Admin\PagesController@pages");

Route::get('/admin/pages/create', "Admin\PagesController@create");

Route::get('/admin/pages/{page}/edit', "Admin\PagesController@edit");

Route::post('/admin/pages/{page}', "Admin\PagesController@update");

Route::delete('/admin/pages/{page}', "Admin\PagesController@destroy");

Route::put('/admin/pages',"Admin\PagesController@store");



Route::get('/pages',"PagesController@pages");

Route::get('/pages/{page}',"PagesController@pagesContents");



Route::get('/users/create', "RegistrationController@create");

Route::put('/users', "RegistrationController@store");

Route::get('/logout', "SessionController@destroy");

Route::get('/login', "SessionController@create");

Route::post('/login', "SessionController@store");



